#include <iostream>

using namespace std;
template<typename T>

class stack
{
public:
    stack() : root(nullptr) {} //�� ��������� ������ ����
    void push(const T& x)
    {
        Node* newnode = new Node;
        newnode->data = x;
        newnode->next = root;
        root = newnode;
    }

    bool empty() const
    {
        return root == nullptr;
    }
    const T& top() const
    {
        if (empty())
        {
            throw length_error("stack is empty");
        }
        return root->data;
    }

    T pop()
    {
        if (empty())
        {
            throw length_error("stack is empty"); //���� ���� ������ ����� � �������
        }
        Node* delnode = root;  //��������� ��������� ����
        T x = delnode->data;   //��������� �������� ������� ����� �������
        root = delnode->next;  //root ��������� �� ���� ����
        delete delnode;        //������� ����
        return x;
    }


private:
    struct Node
    {
        T data;     //���� � �������
        Node* next; //��������� �� ���������
    };
    Node* root;     //������� �����

};

int main()
{
    stack<int> st;
    for (int i = 1; i <= 10; i++)
    {
        st.push(i);
    }
    while (!st.empty())
    {
        cout << st.pop() << " ";
    }

    return 0;
}


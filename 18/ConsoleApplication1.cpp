﻿// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

class Stack
{
private:

    int top;
    int N = 5;
    int* p = new int[N];


public:
    Stack() : top()
    {
        top = 0;
    }

    void push(int var)
    { top += 1;
        if (top < N)
           
            top++;
        p[top] = var;
    }

    int pop()
    {
        int var = p[top];
        if(top>=0)
        top--;
        top -= 1;
        return var;
    }
    ~Stack()
    {
        if (top > 0)
            delete[] p;
    }
};


int main()
{
   

    setlocale(LC_ALL, "RUS");
   
 
    Stack a;
    a.push(1);
    a.push(2);
    a.push(3);

    cout << a.pop() << endl;
    cout << a.pop() << endl;
    cout << a.pop() << endl;
   
    return 0;
}



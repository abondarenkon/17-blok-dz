﻿// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

class Stack
{
private:

    int top;
    int N = 10;
    int* p = new int[N];


public:
    Stack() : top()
    {}

    void push(int var)
    {
        top++;
        p[top] = var;
    }

    int pop()
    {
        int var = p[top];
        top--;
        return var;
    }
    ~Stack()
    {
        if (top > 0)
            delete[] new int;
    }
};


int main()
{
   

    setlocale(LC_ALL, "RUS");
   
 
    Stack a;
    a.push(1);
    a.push(2);
    a.push(3);

    cout << a.pop() << endl;

    cout << a.pop() << endl;

    cout << a.pop() << endl;
    return 0;
}



пишу тут , так как к компьютеру доступа нет. 
#include <iostream>
using namespace std;

class Animal
{
protected:
    std::string m_name;
    // Мы делаем этот конструктор protected так как не хотим, чтобы пользователи имели возможность создавать объекты класса Animal напрямую,
    // но хотим, чтобы в дочерних классах доступ был открыт
    Animal(std::string name)
        : m_name(name)
    {
    }
public:
    std::string getName() { return m_name; }
    virtual const char* speak() { return "nature"; }
};
class Cat: public Animal
{
public:
    Cat(std::string name)
        : Animal(name)
    {
    }
    virtual const char* speak() { return "Meow"; }
};
class Dog: public Animal
{
public:
    Dog(std::string name)
        : Animal(name)
    {
    }
    virtual const char* speak() { return "Woof"; }
};
class Drozd: public Animal
{
	public:
	Drozd(std::string name)
	:Animal(name)
	{
	}
	virtual const char* speak() {return "chirik";}
};
void report(Animal &animal)
{
    std::cout << animal.getName() << " says " << animal.speak() << "\n";
}
int main()
{
    Cat matros("Matros");
Dog barsik("Barsik");
Drozd chij("Chij");
// Создаем массив указателей на наши объекты Cat и Dog
Animal *animals[] = { &matros, &barsik, &chij};
for (int i=0; i < 3; ++i)
{
    std::cout << animals[i]->getName() << " says " << animals[i]->speak() << "\n";
}
    Cat cat("Matros");
    Dog dog("Barsik");
    Drozd drozd("Chij");
    
    report(cat);
    report(dog);
    report(chij);
    
}
